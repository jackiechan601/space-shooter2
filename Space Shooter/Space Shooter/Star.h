#pragma once
// Library Includes
#include <SFML/Graphics.hpp>
#include <vector>
class Star
{
public:
	// Constructor
	Star(sf::Texture& starTexture, sf::Vector2u newScreenBounds);
	// Public Functions
	void Update(sf::Time frameTime);
	void Reset();
	void DrawTo(sf::RenderTarget& target);
private:
	// Variables
	sf::Sprite sprite;
	float speed;
	sf::Vector2u screenBounds;
};

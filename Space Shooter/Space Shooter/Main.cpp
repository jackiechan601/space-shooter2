﻿#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Star.h"
int main()
{
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode(1200, 800), "Space Shooter");

	// Game Setup
	// Player
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());
	
	// Declare a font variable called gameFont
	sf::Font gameFont;
	// Load up the font from a file path
	gameFont.loadFromFile("Assets/Font/mainFont.ttf");
	// Title Text
	// Declare a text variable called titleText to hold our game title display
	sf::Text titleText;
	// Set the font our text should use
	titleText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	titleText.setString("Space Shooter");
	// Set the size of our text, in pixels
	titleText.setCharacterSize(24);
	// Set the colour of our text
	titleText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Jack Greenhill-Lally");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	// Create a time value to store the total time limit for our game
	sf::Time timeLimit = sf::seconds(60.0f);
	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;
	// Seed the random number generator
	srand(time(NULL));

	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star> stars;
	int numStars = 5;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}
	while (gameWindow.isOpen()) 
	{
		// Player keybind input
		playerObject.Input();
		
		// Update Section
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;
		// Update our time display based on our time remaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
		// Update the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].Update(frameTime);
		}
		// Move the player
		playerObject.Update(frameTime);

		// Draw Section
		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);
		// Draw everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(timerText);
		gameWindow.draw(playerObject.sprite);
		// Draw the stars
		for (int i = 0; i < stars.size(); ++i)
		{
			stars[i].DrawTo(gameWindow);
		}
		// Display the window contents on the screen
		gameWindow.display();
	}

	return 0;
}